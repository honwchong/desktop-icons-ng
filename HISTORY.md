# History of versions #

* Version 0.5.0 (2019/10/15)
      * Fix right-click menu in trash not showing sometimes
      * Fix opening a file during New folder operation
      * Changed license to GPLv3 only

* Version 0.4.0 (2019/10/04)
      * Fix Drag'n'Drop in some special cases
      * Don't relaunch the desktop process when disabling and enabling fast
      * Temporary fix for X11 size

* Version 0.3.0 (2019/09/17)
      * Separate Wayland and X11 paths
      * When a file is dropped from another window, it is done at the cursor
      * Fixed bug when dragging several files into a Nautilus window

* Version 0.2.0 (2019/08/19)
      * Shows the full filename if selected
      * Use theme color for selections
      * Sends debug info to the journal
      * Now kills fine old, unneeded processes
      * Allows to launch the desktop app as standalone
      * Ensures that the desktop is kept at background when switching workspaces
      * Honors the Scale value (for retina-like monitors)
      * Hotkeys
      * Check if the desktop folder is writable by others
      * Now the settings window doesn't block the icons
      * Don't show hidden files

* Version 0.1.0 (2019/08/13)
      * First semi-working version version
      * Has everything supported by Desktop Icons, plus Drag'n'Drop
